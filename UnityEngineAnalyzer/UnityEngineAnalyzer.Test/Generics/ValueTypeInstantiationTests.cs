﻿using Linty.Analyzers;
using Linty.Analyzers.Generics;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;
using RoslynNUnitLight;


namespace UnityEngineAnalyzer.Test.Generics
{
    /*
    [TestFixture]
    internal sealed class ValueTypeInstantiationTests : AnalyzerTestFixture
    {
        protected override DiagnosticAnalyzer CreateAnalyzer() => new ValueTypeInstantiationAnalyzer();

        [Test]
        public void ExampleTest()
        {
            const string code = @"
using UnityEngine;

struct SomeStruct
{
    // empty
}

class Program
{
    void Main()
    {
        var someStruct = new SomeStruct();
    }
}
";

            NoDiagnostic(code, DiagnosticIDs.ValueTypeInstantiation);
        }

        [Test]
        public void DiagnosticReportedTest()
        {
            const string code = @"
using UnityEngine;

interface ISomeInterface
{
    void DoStuff();
}

struct SomeStruct : ISomeInterface
{
    public void DoStuff()
    {
        // empty
    }
}

class Program
{
    public static void Main()
    {
        GenericMethod<SomeStruct>();
    }

    void GenericMethod<T>() where T : struct, ISomeInterface
    {
        [|new T()|].DoStuff();
    }
}
";

            HasDiagnostic(code, DiagnosticIDs.ValueTypeInstantiation);
        }

        [Test]
        public void DiagnosticNotReportedTest()
        {
            const string code = @"
using UnityEngine;

interface ISomeInterface
{
    void DoStuff();
}

struct SomeStruct : ISomeInterface
{
    public void DoStuff()
    {
        // empty
    }
}

class Program
{
    public static void Main()
    {
        GenericMethod<SomeStruct>();
    }

    void GenericMethod<T>() where T : struct, ISomeInterface
    {
        default(T).DoStuff();
    }
}
";

            NoDiagnostic(code, DiagnosticIDs.ValueTypeInstantiation);
        }
    }
    */
}
