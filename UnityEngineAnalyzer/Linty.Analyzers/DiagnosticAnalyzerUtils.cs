using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;

namespace Linty.Analyzers
{
    public static class DiagnosticAnalyzerUtils
    {
        private static Action<T> WrappedSafe<T>(Action<T> action)
        {
            void Wrapper(T value)
            {
                try
                {
                    action(value);
                }
                catch (System.Exception ex)
                {
                    System.Console.Error.WriteLine(ex.ToString());
                }
            };

            return Wrapper;
        }

        public static void RegisterSyntaxNodeActionSafe<TLanguageKindEnum>(this AnalysisContext context, Action<SyntaxNodeAnalysisContext> action, params TLanguageKindEnum[] syntaxKinds) where TLanguageKindEnum : struct
        {
            context.RegisterSyntaxNodeAction(WrappedSafe(action), syntaxKinds: syntaxKinds);
        }

        public static void RegisterSymbolActionSafe(this AnalysisContext context, Action<SymbolAnalysisContext> action, params SymbolKind[] symbolKinds)
        {
            context.RegisterSymbolAction(WrappedSafe(action), symbolKinds: symbolKinds);
        }
    }
}
