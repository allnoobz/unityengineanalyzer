using System.Text.RegularExpressions;

namespace Linty.Analyzers.TranslateString
{
    public static class LocalizationManager
    {
        public static readonly Regex PLACEHOLDER =
            new Regex(@"{[a-zA-Z0-9]+}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    }
}