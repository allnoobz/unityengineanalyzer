﻿using Linty.Analyzers;
using Linty.Analyzers.UtilityIntelligence;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;
using RoslynNUnitLight;


namespace UnityEngineAnalyzer.Test.UtilityIntelligence
{
    /*
    [TestFixture]
    sealed class IScoreTagMustHaveEvaluatorNameAnalyzerTests : AnalyzerTestFixture
    {
        protected override DiagnosticAnalyzer CreateAnalyzer() => new IScoreTagMustHaveEvaluatorNameAnalyzer();

        [Test]
        public void TestStructIScoreTag()
        {
            const string code = @"
using UnityEngine;

struct SomeTag [|: IScoreTag<SomeTag>|]
{
    // empty
}";

            HasDiagnostic(code, DiagnosticIDs.IScoreTagMustHaveEvaluatorName);
        }

        [Test]
        public void TestClassIScoreTag()
        {
            const string code = @"
using UnityEngine;


class SomeOtherTag [|: IScoreTag<SomeOtherTag>|]
{
    // empty
}";

            HasDiagnostic(code, DiagnosticIDs.IScoreTagMustHaveEvaluatorName);
        }

        [Test]
        public void TestNonTrivialInterfaceInheritanceIScoreTag()
        {
            const string code = @"
using UnityEngine;

public interface ISomeExtraScoreTag<T> : IScoreTag<T>
{
    // empty
}

class SomeOtherTag [|: ISomeExtraScoreTag<SomeOtherTag>|]
{
    // empty
}";

            HasDiagnostic(code, DiagnosticIDs.IScoreTagMustHaveEvaluatorName);
        }
        
        [Test]
        public void TestIScoreTagWithEvaluatorName()
        {
            const string code = @"
using UnityEngine;

[System.Serializable]
[EvaluatorName(""Has name\"")]
[|class SomeOtherTag : IScoreTag<SomeOtherTag>
{
    // empty
}|]";

            NoDiagnostic(code, DiagnosticIDs.IScoreTagMustHaveEvaluatorName);
        }
        
        [Test]
        public void TestIScoreTagWithEvaluatorNameStruct()
        {
            const string code = @"
using UnityEngine;

[System.Serializable]
[EvaluatorName(""Has name\"")]
[|struct SomeOtherTag : ISomeOtherInterface, IScoreTag<SomeOtherTag>
{
    // empty
}|]";

            NoDiagnostic(code, DiagnosticIDs.IScoreTagMustHaveEvaluatorName);
        }
        
        [Test]
        public void ComplexTest()
        {
            const string code = @"
using UnityEngine;

[System.Serializable]
[EvaluatorName(""Sum"")]
[|public struct SumAggregateFunc : IUnclampedAggregateFunc, IScoreTag<SumAggregateFunc>
{
    public IntermediateScore Aggregate(ref AggregateBuilder builder)
    {
        var count = (float) builder.count;

        var aggregate = 0f;
        for (int i = 0; i < builder.count; i++)
        {
            var baseScore = builder.GetScore(i);
            aggregate += baseScore;
        }

        return builder.ComputeScore<SumAggregateFunc>(aggregate);
    }
}|]";

            NoDiagnostic(code, DiagnosticIDs.IScoreTagMustHaveEvaluatorName);
        }
    }
    */
}
