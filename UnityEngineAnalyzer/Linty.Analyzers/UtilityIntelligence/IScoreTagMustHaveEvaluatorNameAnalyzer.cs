using System.Collections.Immutable;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace Linty.Analyzers.UtilityIntelligence
{
    /*
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class IScoreTagMustHaveEvaluatorNameAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(DiagnosticDescriptors.IScoreTagMustHaveEvaluatorName);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeActionSafe(AnalyzeNode, SyntaxKind.StructDeclaration);
            context.RegisterSyntaxNodeActionSafe(AnalyzeNode, SyntaxKind.ClassDeclaration);
        }

        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            // check if we have a method invocation
            var typeDeclaration = context.Node as TypeDeclarationSyntax;
            if (typeDeclaration == null) { return; }
            
            var typeSymbol = context.SemanticModel.GetDeclaredSymbol(typeDeclaration) as ITypeSymbol;
            if (typeSymbol == null) { return; }

            var implementsIScoreTag = typeSymbol.AllInterfaces.Any(
                i => i.Name == "IScoreTag"
            );
            implementsIScoreTag = implementsIScoreTag || typeSymbol.BaseType?.Name == "IScoreTag";

            var implementsIComplexScoreTag = typeSymbol.AllInterfaces.Any(
                i => i.Name == "IComplexScoreTag"
            );
            implementsIComplexScoreTag = implementsIComplexScoreTag || typeSymbol.BaseType?.Name == "IComplexScoreTag";
            
            var hasEvaluatorNameAttribute = typeSymbol.GetAttributes().Any(
                a => a.AttributeClass.Name == "EvaluatorName" || a.AttributeClass.Name == "EvaluatorNameAttribute"
            );
            
            if (implementsIScoreTag && !implementsIComplexScoreTag && !hasEvaluatorNameAttribute)
            {
                var diagnostic = Diagnostic.Create(DiagnosticDescriptors.IScoreTagMustHaveEvaluatorName, typeDeclaration.BaseList.GetLocation());
                context.ReportDiagnostic(diagnostic);
            }
        }
    }
    */
}
