﻿
namespace Linty.Analyzers
{
    static class DiagnosticCategories
    {
        public const string GC = "GC";
        public const string StringMethods = "String Methods";
        public const string Miscellaneous = "Miscellaneous";
        public const string Performance = "Performance";

        public const string Localization = "Localization";

        public const string AOT = "AOT";

        public const string UtilityIntelligence = "Utility Intelligence";
        public const string Generics = "Generics";
    }
}
