namespace Linty.Analyzers.TranslateString
{
    public static class StringUtils
    {
        public static string ToTitleCase(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            return char.ToUpper(value[0]) + value.Substring(1);
        }

        public static string ToFullTitleCase(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var words = value.Split(' ');
            for (var i = 0; i < words.Length; i++)
            {
                if (words[i].Length == 0)
                    continue;

                var firstChar = char.ToUpper(words[i][0]);
                var rest = "";
                if (words[i].Length > 1)
                {
                    rest = words[i].Substring(1).ToLower();
                }
                words[i] = firstChar + rest;
            }
            return string.Join(" ", words);
        }
    }
}