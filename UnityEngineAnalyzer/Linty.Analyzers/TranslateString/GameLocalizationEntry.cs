using System;
using System.Xml.Serialization;

namespace Linty.Analyzers.TranslateString
{
    [System.Serializable]
    public class GameLocalizationEntry
    {
        public string key;
        public string context;
        public string value;
        public string stacktrace;
        public LocalizationPluralForm[] plurals;

        [XmlIgnore] [NonSerialized] public string fileName = "";
        
        public GameLocalizationEntry ConvertCase(System.Func<string, string> conv)
        {
            var result = new GameLocalizationEntry();
            result.key = conv(key);
            result.context = context;
            result.value = conv(value);
            result.stacktrace = stacktrace;
            if (plurals != null)
            {
                result.plurals = new LocalizationPluralForm[plurals.Length];
                for (var i = 0; i < plurals.Length; i++)
                {
                    result.plurals[i] = plurals[i].ConvertCase(conv);
                }
            }
            return result;
        }

        public GameLocalizationEntry ToUpper()
        {
            return ConvertCase(s => s.ToUpper());
        }

        public GameLocalizationEntry ToTitleCase()
        {
            return ConvertCase(s => s.ToTitleCase());
        }

        public GameLocalizationEntry ToFullTitleCase()
        {
            return ConvertCase(s => s.ToFullTitleCase());
        }
    }
}