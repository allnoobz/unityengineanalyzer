using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace Linty.Analyzers.TranslateString
{
    public static class LocalizationUtils
    {
        public static GameLocalization LoadLocalization(string path)
        {
            GameLocalization result = null;
            
            try
            {
                using (FileStream fileStream = File.Open(path, FileMode.Open, FileAccess.Read))
                {
                    var serializer = new XmlSerializer(typeof(GameLocalization));
                    var value = serializer.Deserialize(fileStream);
                    result = (GameLocalization) value;
                }
            }
            catch (System.IO.FileNotFoundException ex)
            {
                Debug.WriteLine($"Error while loading file: '{path}'");
                Debug.WriteLine(ex);
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine($"Error while loading file: '{path}'");
                Debug.WriteLine(ex);
            }

            return result;
        }

        public static List<GameLocalization> LoadLocalizations(string path, string namePrefix, string extension)
        {
            var result = new List<GameLocalization>();

            path = Path.GetFullPath(path);
            
            var di = new DirectoryInfo(path);
            foreach (var file in di.GetFiles())
            {
                var name = file.Name.ToLower();
                var ext = file.Extension.ToLower();
                if (name.StartsWith(namePrefix.ToLower()) &&
                    ext == extension)
                {
                    var localization = LoadLocalization(file.FullName);
                    if (localization != null)
                        result.Add(localization);
                }
            }

            return result;
        }
    }
}