﻿using System.Collections.Immutable;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace Linty.Analyzers.AOT
{
    /*
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class IScoreTagMustHaveEvaluatorName : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(DiagnosticDescriptors.IScoreTagMustHaveEvaluatorName);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.StructDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.ClassDeclaration);
        }

        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            // check if we have a method invocation
            var typeDeclaration = context.Node as TypeDeclarationSyntax;
            var typeSymbol = context.SemanticModel.GetSymbolInfo(typeDeclaration).Symbol as ITypeSymbol;
            if (typeSymbol == null) { return; }

            var implementsIScoreTag = typeSymbol.AllInterfaces.Any(
                i => i.Name == "IScoreTag"
            );
            var hasEvaluatorNameAttribute = typeSymbol.GetAttributes().Any(
                a => a.AttributeClass.Name == "EvaluatorName"
            );
            
            if (implementsIScoreTag && !hasEvaluatorNameAttribute)
            {
                var diagnostic = Diagnostic.Create(DiagnosticDescriptors.IScoreTagMustHaveEvaluatorName, typeDeclaration.GetLocation());
                context.ReportDiagnostic(diagnostic);
            }
        }
    }
    */
}
