﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Linty.Analyzers;
using Linty.Analyzers.TranslateString;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;
using RoslynNUnitLight;

namespace UnityEngineAnalyzer.Test.TranslateString
{
    
    [TestFixture]
    sealed class TranslateStringAnalyzerTests : AnalyzerTestFixture
    {
        protected override DiagnosticAnalyzer CreateAnalyzer() => new TranslateStringAnalyzer("../../../../../../Assets/Extra/Locales");

        private void SetupCWD()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            path = Path.GetDirectoryName(path);
            Debug.Assert(path != null);
            Directory.SetCurrentDirectory(path);
        }
        
        [Test]
        public void SetTextMissingTranslation()
        {
            SetupCWD();
            
            const string code = @"
using UnityEngine;


public class C : MonoBehaviour
{
    public TextMeshProUGUI text;

    void Update()
    {
        [|text.SetText(""4231f23tgrg213"")|];
    }
}
";

            HasDiagnostic(code, DiagnosticIDs.TranslateStringMissing);
            NoDiagnostic(code, DiagnosticIDs.TranslateStringVar);
        }
        
        [Test]
        public void SetTextTranslationVar()
        {
            SetupCWD();
            
            const string code = @"
using UnityEngine;


public class C : MonoBehaviour
{
    public TextMeshProUGUI text;

    void Update()
    {
        var x = Random.Range(5) > 2 ? ""Start"" : ""Stop"";
        [|text.SetText(x)|];
    }
}
";

            HasDiagnostic(code, DiagnosticIDs.TranslateStringVar);
            NoDiagnostic(code, DiagnosticIDs.TranslateStringMissing);
        }
        
        [Test]
        public void SetTextOkay()
        {
            SetupCWD();
            
            const string code = @"
using UnityEngine;


public class C : MonoBehaviour
{
    public TextMeshProUGUI text;

    void Update()
    {
        text.SetText(""Start"");
    }
}
";

            NoDiagnostic(code, DiagnosticIDs.TranslateStringMissing);
            NoDiagnostic(code, DiagnosticIDs.TranslateStringVar);
        }

        [Test]
        public void SetTextWithoutTranslationConstOkay()
        {
            const string code = @"
using UnityEngine;


public class C : MonoBehaviour
{
    public TextMeshProUGUI text;

    void Update()
    {
        text.SetTextWithoutTranslation(""123g5t34tg3514"");
    }
}
";

            NoDiagnostic(code, DiagnosticIDs.TranslateStringMissing);
            NoDiagnostic(code, DiagnosticIDs.TranslateStringVar);
        }
        
        [Test]
        public void SetTextWithoutTranslationVarOkay()
        {
            SetupCWD();
            
            const string code = @"
using UnityEngine;


public class C : MonoBehaviour
{
    public TextMeshProUGUI text;

    void Update()
    {
        var x = Random.Range(5) > 2 ? ""Start"" : ""Stop"";
        text.SetTextWithoutTranslation(x);
    }
}
";

            NoDiagnostic(code, DiagnosticIDs.TranslateStringVar);
            NoDiagnostic(code, DiagnosticIDs.TranslateStringMissing);
        }
    }
}
