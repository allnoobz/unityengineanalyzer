using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;


namespace Linty.Analyzers.UtilityIntelligence
{
    /*
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class EvaluatorNameWithoutIScoreTagAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(DiagnosticDescriptors.EvaluatorNameWithoutIScoreTag);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeActionSafe(AnalyzeNode, SyntaxKind.AttributeList);
        }

        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            // check if we have a method invocation
            var attributeList = context.Node as AttributeListSyntax;

            if (attributeList == null) { return; }
            var target = attributeList.Parent;

            if (target == null) { return; }
        
            var typeSymbol = context.SemanticModel.GetDeclaredSymbol(target) as ITypeSymbol;
            if (typeSymbol == null) { return; }

            var implementsIScoreTag = typeSymbol.AllInterfaces.Any(
                i => i.Name == "IScoreTag"
            );
            implementsIScoreTag = implementsIScoreTag || typeSymbol.BaseType?.Name == "IScoreTag";

            var implementsIComplexScoreTag = typeSymbol.AllInterfaces.Any(
                i => i.Name == "IComplexScoreTag"
            );
            implementsIComplexScoreTag = implementsIComplexScoreTag || typeSymbol.BaseType?.Name == "IComplexScoreTag";
        
            var hasEvaluatorNameAttribute = typeSymbol.GetAttributes().Any(
                a => a.AttributeClass.Name == "EvaluatorName" || a.AttributeClass.Name == "EvaluatorNameAttribute"
            );
        
            if (hasEvaluatorNameAttribute && (!implementsIScoreTag || implementsIComplexScoreTag))
            {
                Diagnostic diagnostic;
                var attribute = attributeList.Attributes.First(el => el.Name.ToString() == "EvaluatorName" || el.Name.ToString() == "EvaluatorNameAttribute");
                if (attribute == null)
                {
                    diagnostic = Diagnostic.Create(DiagnosticDescriptors.EvaluatorNameWithoutIScoreTag, attributeList.GetLocation());
                }
                else
                {
                    diagnostic = Diagnostic.Create(DiagnosticDescriptors.EvaluatorNameWithoutIScoreTag, attribute.Parent.GetLocation());
                }
                context.ReportDiagnostic(diagnostic);
            }     
        }
    }
    */
}
