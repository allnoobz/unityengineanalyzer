using System;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;


namespace Linty.Analyzers.Generics
{
    /*
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class ValueTypeInstantiationAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
            ImmutableArray.Create(DiagnosticDescriptors.ValueTypeInstantiation);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.ObjectCreationExpression);
        }

        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            if (!(context.Node is ObjectCreationExpressionSyntax node))
                return;

            var symbolInfo = context.SemanticModel.GetSymbolInfo(node.Type);
            if (!(symbolInfo.Symbol is ITypeParameterSymbol typeParameterSymbol))
                return;

            if (!typeParameterSymbol.HasValueTypeConstraint)
                return;

            context.ReportDiagnostic(
                Diagnostic.Create(
                    DiagnosticDescriptors.ValueTypeInstantiation,
                    context.Node.GetLocation()));
        }
    }
    */
}
