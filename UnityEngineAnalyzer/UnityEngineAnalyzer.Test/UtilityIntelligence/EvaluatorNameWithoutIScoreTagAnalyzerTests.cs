﻿using Linty.Analyzers;
using Linty.Analyzers.UtilityIntelligence;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;
using RoslynNUnitLight;


namespace UnityEngineAnalyzer.Test.UtilityIntelligence
{
    /*
    [TestFixture]
    sealed class EvaluatorNameWithoutIScoreTagAnalyzerTests : AnalyzerTestFixture
    {
        protected override DiagnosticAnalyzer CreateAnalyzer() => new EvaluatorNameWithoutIScoreTagAnalyzer();

        [Test]
        public void TestStructEvaluatorName()
        {
            const string code = @"
using UnityEngine;

[|[EvaluatorName]|]
struct SomeTag
{
    // empty
}";

            HasDiagnostic(code, DiagnosticIDs.EvaluatorNameWithoutIScoreTag);
        }

        [Test]
        public void TestClassEvaluatorName()
        {
            const string code = @"
using UnityEngine;


[|[EvaluatorName]|]
class SomeTag
{
    // empty
}";

            HasDiagnostic(code, DiagnosticIDs.EvaluatorNameWithoutIScoreTag);
        }

        [Test]
        public void TestNegative()
        {
            const string code = @"
using UnityEngine;

[|[EvaluatorName]|]
class SomeOtherTag : IScoreTag<T>
{
    // empty
}";

            NoDiagnostic(code, DiagnosticIDs.EvaluatorNameWithoutIScoreTag);
        }

        [Test]
        public void TestEvaluatorNameOnComplexScoreTag()
        {
            const string code = @"
using UnityEngine;

[System.Serializable]
[|[EvaluatorName(""Has name"")]|]
class SomeOtherTag : IComplexScoreTag<SomeOtherTag>
{
    // empty
}";

            HasDiagnostic(code, DiagnosticIDs.EvaluatorNameWithoutIScoreTag);
        }
        
        [Test]
        public void TestEvaluatorNameOnInvalidTarget()
        {
            const string code = @"
using UnityEngine;

[System.Serializable]
class SomeOtherTag
{
    [EvaluatorName(""Blah"")]
    public float x;
}";

            NoDiagnostic(code, DiagnosticIDs.EvaluatorNameWithoutIScoreTag);
        }
        
        [Test]
        public void TestInterfaceTag()
        {
            const string code = @"
[Injectable]
public interface ICameraManager
{
    // empty
}";

            NoDiagnostic(code, DiagnosticIDs.EvaluatorNameWithoutIScoreTag);
        }
    }
    */
}
