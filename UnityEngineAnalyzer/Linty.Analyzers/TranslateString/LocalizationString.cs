using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Linty.Analyzers.TranslateString
{
    public class LocalizationString : System.IComparable<LocalizationString>
    {
        public readonly string key;
        private readonly string comparisonKey;
        public readonly string context;
        public readonly string originFileName;

        private string MakeComparisonKey(string key)
        {
            key = key.Trim('\r', '\n', ' ', '\t');
            var sb = new StringBuilder();
            var matches = LocalizationManager.PLACEHOLDER.Matches(key);
            var index = 0;
            var subs = new Dictionary<string, string>();
            var maxKey = 0;
            foreach (Match m in matches)
            {
                sb.Append(key.Substring(index, m.Index - index));
                var placeholder = m.Value;
                if (!subs.TryGetValue(placeholder, out var placeholderSub))
                {
                    placeholderSub = "{" + maxKey + "}";
                    maxKey++;
                    subs.Add(placeholder, placeholderSub);
                }
                sb.Append(placeholderSub);
                index = m.Index + m.Length;
            }
            sb.Append(key.Substring(index, key.Length - index));
            return sb.ToString();
        }

        public LocalizationString(string key, string context, string originFileName)
        {
            this.key = key;
            this.comparisonKey = MakeComparisonKey(key);
            this.context = context;
            this.originFileName = originFileName;
        }
        
        public LocalizationString ToUpper()
        {
            return new LocalizationString(key.ToUpper(), context, originFileName);
        }

        public LocalizationString ToTitleCase()
        {
            return new LocalizationString(key.ToTitleCase(), context, originFileName);
        }

        public LocalizationString ToFullTitleCase()
        {
            return new LocalizationString(key.ToFullTitleCase(), context, originFileName);
        }

        public override bool Equals(object obj)
        {
            var other = obj as LocalizationString;
            if (other == null)
            {
                return false;
            }

            return other.comparisonKey == comparisonKey && other.context == context;
        }

        public override int GetHashCode()
        {
            return unchecked(comparisonKey.GetHashCode() * 13 + (context == null ? 0 : context.GetHashCode()) * 17 + 3);
        }

        public static bool operator ==(LocalizationString x, LocalizationString y)
        {
            if (object.ReferenceEquals(x, null))
            {
                return object.ReferenceEquals(y, null);
            }
            return x.Equals(y);
        }

        public static bool operator !=(LocalizationString x, LocalizationString y)
        {
            return !(x == y);
        }

        public int CompareTo(LocalizationString other)
        {
            var ctx = this.context ?? "";
            var otherCtx = other.context ?? "";
            var ctxComparison = ctx.CompareTo(otherCtx);
            if (ctxComparison != 0)
            {
                return ctxComparison;
            }
            var k = comparisonKey ?? "";
            var otherK = other.comparisonKey ?? "";
            return k.CompareTo(otherK);
        }
    }
}