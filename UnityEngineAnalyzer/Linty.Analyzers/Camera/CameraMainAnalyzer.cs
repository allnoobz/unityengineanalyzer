﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace Linty.Analyzers.Camera
{
    /*
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class CameraMainAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(DiagnosticDescriptors.CameraMainIsSlow);
        private List<MemberAccessExpressionSyntax> searched = new List<MemberAccessExpressionSyntax>();
        private List<MethodDeclarationSyntax> visited = new List<MethodDeclarationSyntax>();

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeActionSafe(AnalyzeClassSyntax, SyntaxKind.ClassDeclaration);
        }

        public void AnalyzeClassSyntax(SyntaxNodeAnalysisContext context)
        {
            var monoBehaviourInfo = new MonoBehaviourInfo(context);

            if (!monoBehaviourInfo.IsMonoBehaviour())
            {
                return;
            }
            
            monoBehaviourInfo.ForEachUpdateMethod((updateMethod) =>
            {
                SearchCameraMain(context, updateMethod);
                RecursiveMethodCrawler(context, updateMethod, 5);
            });
        }

        private void SearchCameraMain(SyntaxNodeAnalysisContext context, MethodDeclarationSyntax method)
        {
            var memberAccessExpression = method.DescendantNodes().OfType<MemberAccessExpressionSyntax>();

            foreach (var memberAccess in memberAccessExpression)
            {
                if (searched.Contains(memberAccess))
                {
                    return;
                }

                searched.Add(memberAccess);

                SymbolInfo symbolInfo;
                if (!context.TryGetSymbolInfo(memberAccess.Expression, out symbolInfo))
                {
                    continue;
                }

                var containingClass = symbolInfo.Symbol?.ContainingType;

                if (containingClass != null && containingClass.ContainingNamespace.Name.Equals("UnityEngine") && containingClass.Name.Equals("Camera"))
                {
                    if (symbolInfo.Symbol.Name.Equals("main"))
                    {
                        var diagnostic = Diagnostic.Create(DiagnosticDescriptors.CameraMainIsSlow, memberAccess.GetLocation(), memberAccess.Name, symbolInfo.Symbol.Name);
                        context.ReportDiagnostic(diagnostic);
                    }
                }
            }
        }

        private object lastMethodRecursion = null;

        private void RecursiveMethodCrawler(SyntaxNodeAnalysisContext context, MethodDeclarationSyntax method, int depth)
        {
            if (depth < 0)
            {
                return;
            }

            if (visited.Contains(method))
            {
                return;
            }
            visited.Add(method);
            
            var invocationAccessExpression = method.DescendantNodes().OfType<InvocationExpressionSyntax>();
            SearchCameraMain(context, method);

            foreach (var invocation in invocationAccessExpression)
            {
                if (lastMethodRecursion == invocationAccessExpression)
                {
                    break;
                }

                lastMethodRecursion = invocationAccessExpression;

                SymbolInfo symbolInfo;
                if (!context.TryGetSymbolInfo(invocation, out symbolInfo))
                {
                    continue;
                }

                var methodSymbol = symbolInfo.Symbol as IMethodSymbol;

                if (methodSymbol == null)
                {
                    return;
                }

                var methodDeclarations = methodSymbol.DeclaringSyntaxReferences;

                foreach (var methodDeclaration in methodDeclarations)
                {
                    var theMethodSyntax = methodDeclaration.GetSyntax() as MethodDeclarationSyntax;
                    RecursiveMethodCrawler(context, theMethodSyntax, depth - 1);
                }
            }
        }
    }
    */
}
