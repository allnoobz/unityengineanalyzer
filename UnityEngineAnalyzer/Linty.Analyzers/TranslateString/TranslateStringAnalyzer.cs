﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace Linty.Analyzers.TranslateString
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class TranslateStringAnalyzer : DiagnosticAnalyzer
    {
        private List<MemberAccessExpressionSyntax> searched = new List<MemberAccessExpressionSyntax>();
        private List<MethodDeclarationSyntax> visited = new List<MethodDeclarationSyntax>();

        private readonly string localePath = "./Assets/Extra/Locales/";
        private readonly string localeNamePrefix = "en_US";
        private readonly string localeNameExt = ".locale";
        
        private List<GameLocalization> localizations;

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(new[]
        {
            DiagnosticDescriptors.TranslateStringMissing,
            DiagnosticDescriptors.TranslateStringVar,
        });

        public TranslateStringAnalyzer() {}
        
        public TranslateStringAnalyzer(string localePath, string localeNamePrefix = "en_US", string localeNameExt = ".locale")
        {
            this.localePath = localePath;
            this.localeNamePrefix = localeNamePrefix;
            this.localeNameExt = localeNameExt;
        }
        
        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeActionSafe(AnalyzeMethodSyntax, SyntaxKind.MethodDeclaration);

            localizations = LocalizationUtils.LoadLocalizations(localePath, localeNamePrefix, localeNameExt);
            foreach (var loc in localizations)
            {
                loc.Init();
            }
        }
        
        public void AnalyzeMethodSyntax(SyntaxNodeAnalysisContext context)
        {
            var method = context.Node as MethodDeclarationSyntax;
            Debug.Assert(method != null);
            
            var invocations = method.DescendantNodes().OfType<InvocationExpressionSyntax>();

            foreach (var invocation in invocations)
            {
                if (invocation.MethodName() != "SetText")
                    continue;
                if (invocation.ArgumentList.IsMissing)
                    continue;

                var childNodes = invocation.ArgumentList.Arguments;
                if (childNodes.Count == 0)
                    continue;

                var firstChild = childNodes[0];
                
                var constStr = context.SemanticModel.GetConstantValue(firstChild.Expression);
                if (!constStr.HasValue || !(constStr.Value is string))
                {
                    var diagnostic = Diagnostic.Create(DiagnosticDescriptors.TranslateStringVar, invocation.GetLocation(), invocation.MethodName());
                    context.ReportDiagnostic(diagnostic);
                }

                var str = (string) constStr.Value;
                var hasTranslation = false;
                foreach (var loc in localizations)
                {
                    if (loc.HasTranslation(str))
                        hasTranslation = true;
                }

                if (!hasTranslation)
                {
                    var diagnostic = Diagnostic.Create(DiagnosticDescriptors.TranslateStringMissing, invocation.GetLocation(), invocation.MethodName(), str);
                    context.ReportDiagnostic(diagnostic);
                }
            }
        }
    }
}
