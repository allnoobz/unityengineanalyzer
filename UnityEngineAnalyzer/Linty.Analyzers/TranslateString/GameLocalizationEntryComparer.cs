using System.Collections.Generic;

namespace Linty.Analyzers.TranslateString
{
    public class GameLocalizationEntryComparer : IComparer<GameLocalizationEntry>
    {
        public int Compare(GameLocalizationEntry x, GameLocalizationEntry y)
        {
            return x.key.CompareTo(y.key);
        }
    }
}