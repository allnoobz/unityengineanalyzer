using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Linty.Analyzers.TranslateString
{
    [System.Serializable]
    public class GameLocalization
    {
        public GameLocalizationEntry[] entries;

        private Dictionary<LocalizationString, GameLocalizationEntry> entryDict;

        public GameLocalization()
        {
            entries = new GameLocalizationEntry[] { };
        }

        public static GameLocalization MergeLocalizations(IEnumerable<GameLocalization> localizations)
        {
            var entriesSum = new List<GameLocalizationEntry>();
            foreach (var localization in localizations)
            {
                entriesSum.AddRange(localization.entries);
            }
            
            var duplicates = entriesSum.GroupBy(x => x.key + "|$#$\\" + x.context)
                .Where(g => g.Count() > 1)
                .Select(g => Regex.Split(g.Key, @"\|\$#\$\\"))
                .ToList();
            return new GameLocalization {entries = entriesSum.ToArray()};
        }

        public void Init()
        {
            entryDict = new Dictionary<LocalizationString, GameLocalizationEntry>();
            try
            {
                for (var i = 0; i < entries.Length; i++)
                {
                    var entry = entries[i];
                    if (entry.context == "")
                    {
                        entry.context = null;
                    }

                    var key = new LocalizationString(entry.key, entry.context, entry.fileName);
                    entryDict[key] = entry;
                    var upperKey = key.ToUpper();
                    if (upperKey != key)
                    {
                        entryDict[upperKey] = entry.ToUpper();
                    }

                    var titleKey = key.ToTitleCase();
                    if (titleKey != key && titleKey != upperKey)
                    {
                        entryDict[titleKey] = entry.ToTitleCase();
                    }

                    var fullTitleKey = key.ToFullTitleCase();
                    if (fullTitleKey != key && fullTitleKey != titleKey && fullTitleKey != upperKey)
                    {
                        entryDict[fullTitleKey] = entry.ToFullTitleCase();
                    }
                }
            }
            catch (System.Exception ex)
            {
                entryDict.Clear();
            }
        }

        public bool HasTranslation(string key, string context = null)
        {
            return entryDict.ContainsKey(new LocalizationString(key, context, null));
        }
    }
}