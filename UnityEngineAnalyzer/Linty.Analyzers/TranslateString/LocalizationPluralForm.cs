namespace Linty.Analyzers.TranslateString
{
    [System.Serializable]
    public class LocalizationPluralForm
    {
        public string value;
        public string argName;
        public string condition;

        public LocalizationPluralForm ConvertCase(System.Func<string, string> conv)
        {
            return new LocalizationPluralForm
            {
                value = conv(value),
                argName = conv(argName), // FIXME: Preserve argument case
                condition = condition,
            };
        }
    }
}